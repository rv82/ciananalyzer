﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using CianAnalyzer.Data.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CianAnalyzer.Data.Models;
using System.Data.Entity;
using Moq;

namespace CianAnalyzer.Data.Services.Tests
{
    [TestClass()]
    public class LinksServiceTests
    {
        private LinksService linksService;
        private Mock<AppDbContext> mockContext;

        [TestInitialize]
        public void TestsSetup()
        //public LinksServiceTests()
        {
            List<SitemapRealtyPageLink> realtyPageLinksList = new List<SitemapRealtyPageLink>
            {
                new SitemapRealtyPageLink{Id=1, LinkText="Link1", LinkUrl="http://url1", IsProcessed=true},
                new SitemapRealtyPageLink{Id=2, LinkText="Link2", LinkUrl="http://url2"},
                new SitemapRealtyPageLink{Id=3, LinkText="Link3", LinkUrl="http://url3"}
            };
            var mockSet = new Mock<DbSet<SitemapRealtyPageLink>>().SetupData(realtyPageLinksList);
            mockContext = new Mock<AppDbContext>();
            mockContext.Setup(c => c.SitemapRealtyPageLinks).Returns(mockSet.Object);
            linksService = new LinksService(mockContext.Object);
        }

        [TestMethod()]
        [Description("Тестирование метода GetOneUnprocessedRealtyPageLinkAndMarkAsInProcessing")]
        public void GetOneUnprocessedSitemapRealtyPageLinkAndMarkAsInProcessingTest()
        {
            const int id = 2;

            // Получаем объект.
            var realtyPageLink = linksService.GetOneUnprocessedSitemapRealtyPageLinkAndMarkAsInProcessing();
            Assert.IsNotNull(realtyPageLink);
            Assert.IsTrue(realtyPageLink is SitemapRealtyPageLink);
            Assert.IsTrue(realtyPageLink.Id == id);
            Assert.IsTrue(realtyPageLink.InProcessing);

            // Проверяем, изменилось ли состояние значение InProcessing.
            realtyPageLink = mockContext.Object.SitemapRealtyPageLinks.First(link => link.Id == id);
            Assert.IsNotNull(realtyPageLink);
            Assert.IsTrue(realtyPageLink.InProcessing);
        }

        [TestMethod()]
        [Description("Тестирование метода GetOneUnprocessedRealtyPageLinkAndMarkAsInProcessing")]
        public void MarkSitemapRealtyPageLinkAsProcessedTest()
        {
            const int id = 3;

            // Проверяем значение IsProcessed для объекта с Id=3. Должно быть false
            SitemapRealtyPageLink link = mockContext.Object.SitemapRealtyPageLinks.FirstOrDefault(l => l.Id == 3);
            Assert.IsNotNull(link);
            Assert.IsFalse(link.IsProcessed);

            // Устанавливаем IsProcessed для объекта с Id=3.
            int result = linksService.MarkSitemapRealtyPageLinkAsProcessed(id);
            Assert.IsTrue(result != -1);

            // Проверяем значение IsProcessed для объекта с Id=3. Должно быть true
            link = mockContext.Object.SitemapRealtyPageLinks.FirstOrDefault(l => l.Id == id);
            Assert.IsNotNull(link);
            Assert.IsTrue(link.IsProcessed);
        }

        [TestMethod()]
        [Description("Тестирование методов GetOneUnprocessedSitemapRealtyPageLink и MarkSitemapRealtyPageLinkAsInProcessing")]
        public void MarkSitemapRealtyPageLinkAsNotInProcessingTest()
        {
            const int id = 2;

            // Получаем объект.
            var realtyPageLink = linksService.GetOneUnprocessedSitemapRealtyPageLinkAndMarkAsInProcessing();
            Assert.IsNotNull(realtyPageLink);
            Assert.IsTrue(realtyPageLink is SitemapRealtyPageLink);
            Assert.IsTrue(realtyPageLink.Id == id);
            Assert.IsTrue(realtyPageLink.InProcessing);

            // Устанавливаем InProcessing в false
            int result = linksService.MarkSitemapRealtyPageLinkAsNotInProcessing(realtyPageLink.Id);
            Assert.IsTrue(result != -1);
            Assert.IsFalse(realtyPageLink.InProcessing);
        }
    }
}