﻿using CianAnalyzer.Components.Helpers;
using AngleSharp.Dom.Html;
using AngleSharp.Parser.Html;
using CianAnalyzer.Data.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using static CianAnalyzer.Components.Constants;

namespace CianAnalyzer.Components.Helpers.Tests
{
    [TestClass()]
    public class UrlHelperTests
    {
        [TestMethod()]
        [Description("Проверка работы метода GetPageIdentifierFromUrl")]
        [DataRow("https://ivanovo.cian.ru/rent/suburban/684093978/", 684093978)]
        [DataRow("https://ivanovo.cian.ru/rent/suburban/680449169/", 680449169)]
        public void GetPageIdentifierFromUrlTest(string url, long id)
        {
            long idFromUrl = UrlHelper.GetPageIdentifierFromUrl(url);
            Assert.AreEqual(id, idFromUrl);
        }

        [TestMethod()]
        [Description("Проверка работы метода GetAnchorPathsFromDocument")]
        // Разбор ссылок со страницы sitemap из блока выбора страниц (pager_pages)
        [DataRow("<div class=\"pager_pages\"><a href=\"/sitemap/\">1</a><a href=\"/sitemap2/\">2</a></div>", "sitemap", "sitemap2", HtmlElementSelectors.LinkInPagerOnSitemapClassName, 2)]
        // Разбор ссылок на  со страницы sitemap
        [DataRow("<div class=\"note objects\"><div style=\"padding: 20px\"><a href=\"/kupit-komnatu-moskva-metro-aviamotornaya/\"></a><a href=\"/kupit-komnatu-moskva-szao-041/\"></a></div></div>", "kupit-komnatu-moskva-metro-aviamotornaya", "kupit-komnatu-moskva-szao-041", HtmlElementSelectors.LinkFromSitemapClassName, 2)]
        // Разбор ссылок со страницы предложений, из блока list--35Suf
        [DataRow("<ul class=\"list--35Suf\"><li class=\"list-item--2QgXB list-item--active--2-sVo\"><span>1</span></li><li class=\"list-item--2QgXB\"><a href=\"https://www.cian.ru/cat.php?deal_type=sale&amp;district%5B0%5D=1&amp;engine_version=2&amp;offer_type=flat&amp;p=2&amp;room0=1\" class=\"list-itemLink--39icE\">2</a></li><li class=\"list-item--2QgXB\"><a href=\"https://www.cian.ru/cat.php?deal_type=sale&amp;district%5B0%5D=1&amp;engine_version=2&amp;offer_type=flat&amp;p=3&amp;room0=1\" class=\"list-itemLink--39icE\">3</a></li></ul>",
            "cat.php?deal_type=sale&district%5B0%5D=1&engine_version=2&offer_type=flat&p=2&room0=1",
            "cat.php?deal_type=sale&district%5B0%5D=1&engine_version=2&offer_type=flat&p=3&room0=1",
            HtmlElementSelectors.RealtyPageLinkClassName, 2)]
        public void GetLinkSegmentsFromDocumentBySelectorTest(string html, string firstSegment, string lastSegment, string selector, int count)
        {
            HtmlParser parser = new HtmlParser();
            IHtmlDocument doc = parser.Parse(html);
            List<string> sitemapUrlSegments = UrlHelper.GetLinkSegmentsFromDocumentBySelector(doc, selector);
            Assert.IsTrue(sitemapUrlSegments.Count == count);
            Assert.AreEqual(sitemapUrlSegments.First(), firstSegment);
            Assert.AreEqual(sitemapUrlSegments.Last(), lastSegment);
        }

        [TestMethod()]
        [Description("Проверка работы метода GetTargetLinksFromPage")]
        [DataRow("<a class=\"cardLink--3KbME\" target=\"_blank\" href=\"https://www.cian.ru/sale/flat/163102851/\">Подробнее</a><a class=\"cardLink--3KbME\" target=\"_blank\" href=\"https://www.cian.ru/sale/flat/154233743/\">Подробнее</a>", 2, 163102851, 154233743)]
        public void GetRealtyPageUrlIdentifiersFromPageTest(string pageText, int count, long firstId, long lastId)
        {
            HtmlParser parser = new HtmlParser();
            IHtmlDocument page = parser.Parse(pageText);
            List<long> links = UrlHelper.GetRealtyPageUrlIdentifiersFromPage(page);
            Assert.IsTrue(links.Count == count);
            Assert.AreEqual(firstId, links.First());
            Assert.AreEqual(lastId, links.Last());
        }

        [TestMethod()]
        [Description("Проверка работы метода GetAnchorPathsFromDocument")]
        // Разбор ссылок со страницы sitemap
        [DataRow("<div class=\"note objects\"><div style=\"padding: 20px\"><a href=\"/kupit-komnatu-moskva-metro-aviamotornaya/\"></a><a href=\"/kupit-komnatu-moskva-szao-041/\"></a></div></div>", "kupit-komnatu-moskva-metro-aviamotornaya", "kupit-komnatu-moskva-szao-041", HtmlElementSelectors.LinkFromSitemapClassName, 2)]
        public void GetLinkObjectsFromDocumentBySelectorTest(string html, string firstSegment, string lastSegment, string selector, int count)
        {
            HtmlParser parser = new HtmlParser();
            IHtmlDocument doc = parser.Parse(html);
            List<SitemapRealtyPageLink> sitemapLinks = UrlHelper.GetLinkObjectsFromDocumentBySelector(doc, selector);
            Assert.IsTrue(sitemapLinks.Count == count);
            Assert.AreEqual(sitemapLinks.First().LinkUrl, string.Join("/", Urls.BaseUrl, firstSegment));
            Assert.AreEqual(sitemapLinks.Last().LinkUrl, string.Join("/", Urls.BaseUrl, lastSegment));
        }

        [TestMethod()]
        [Description("Проверка работы метода GetRealtyPageUrlObjectFromPage")]
        [DataRow("<a class=\"cardLink--3KbME\" target=\"_blank\" href=\"https://www.cian.ru/sale/flat/163102851/\">Подробнее</a><a class=\"cardLink--3KbME\" target=\"_blank\" href=\"https://www.cian.ru/sale/flat/154233743/\">Подробнее</a>",
            2,
            "https://www.cian.ru/sale/flat/163102851/",
            "https://www.cian.ru/sale/flat/154233743/",
            163102851, 154233743)]
        public void GetRealtyPageUrlObjectFromPageTest(string pageText, int count, string url1, string url2, long firstId, long lastId)
        {
            HtmlParser parser = new HtmlParser();
            IHtmlDocument page = parser.Parse(pageText);
            List<RealtyPageUrl> links = UrlHelper.GetRealtyPageUrlsFromPage(page);
            Assert.IsTrue(links.Count == count);
            Assert.AreEqual(url1,links.First().PageUrl);
            Assert.AreEqual(url2, links.Last().PageUrl);
            Assert.AreEqual(firstId, links.First().PageIdentifier);
            Assert.AreEqual(lastId, links.Last().PageIdentifier);
        }
    }
}