namespace CianAnalyzer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.RealtyPageUrls",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        PageIdentifier = c.Long(nullable: false),
                        PageUrl = c.String(nullable: false, maxLength: 200),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.SitemapRealtyPageLinks",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        LinkText = c.String(maxLength: 300),
                        LinkUrl = c.String(nullable: false, maxLength: 350),
                        InProcessing = c.Boolean(nullable: false),
                        IsProcessed = c.Boolean(nullable: false),
                        SitemapUrl = c.String(maxLength: 200),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.SitemapRealtyPageLinks");
            DropTable("dbo.RealtyPageUrls");
        }
    }
}
