﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CianAnalyzer.Data.Models;
using static CianAnalyzer.Components.Constants;

namespace CianAnalyzer.Data
{
    public class AppDbContext: DbContext
    {
        public AppDbContext() : base(ConnectionName) { }

        /// <summary>
        /// Ссылки на страницы
        /// </summary>
        public virtual DbSet<RealtyPageUrl> RealtyPageUrls { get; set; }

        /// <summary>
        /// Обработанные ссылки
        /// </summary>
        public virtual DbSet<SitemapRealtyPageLink> SitemapRealtyPageLinks { get; set; }
    }
}
