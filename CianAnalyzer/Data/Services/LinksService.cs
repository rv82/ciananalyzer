﻿using CianAnalyzer.Data.Models;
using System.Collections.Generic;
using System.Linq;

namespace CianAnalyzer.Data.Services
{
    public class LinksService
    {
        private AppDbContext context;

        public LinksService(AppDbContext dbContext)
        {
            context = dbContext;
        }

        /// <summary>
        /// Получает из БД объект ссылки, у которого InProcessing и IsProcessed не установлены,
        /// устанавливает значение InProcessing в true и возвращает этот объект.
        /// </summary>
        /// <returns>Объект RealtyPageLink</returns>
        public SitemapRealtyPageLink GetOneUnprocessedSitemapRealtyPageLinkAndMarkAsInProcessing()
        {
            SitemapRealtyPageLink sitemapRealtyPageLink = context.SitemapRealtyPageLinks.FirstOrDefault(link => !link.InProcessing && !link.IsProcessed);
            sitemapRealtyPageLink.InProcessing = true;
            context.SaveChanges();
            return sitemapRealtyPageLink;
        }

        public int MarkSitemapRealtyPageLinkAsNotInProcessing(long id)
        {
            SitemapRealtyPageLink sitemapRealtyPageLink = context.SitemapRealtyPageLinks.FirstOrDefault(link => link.Id == id);
            if (sitemapRealtyPageLink == null)
            {
                return -1;
            }
            sitemapRealtyPageLink.InProcessing = false;
            return context.SaveChanges();
        }

        /// <summary>
        /// Помечает объект RealtyPageLink, как обработанный.
        /// </summary>
        /// <param name="id">Идентификатор записи в БД</param>
        /// <returns>1, если запись изменена и 0, если нет</returns>
        public int MarkSitemapRealtyPageLinkAsProcessed(long id)
        {
            SitemapRealtyPageLink sitemapRealtyPageLink = context.SitemapRealtyPageLinks.FirstOrDefault(link => link.Id == id);
            if (sitemapRealtyPageLink == null)
            {
                return -1;
            }
            sitemapRealtyPageLink.IsProcessed = true;
            return context.SaveChanges();
        }

        /// <summary>
        /// Сохраняет коллекцию объектов RealtyPageUrl в БД.
        /// </summary>
        /// <param name="urls"></param>
        /// <returns>Количество сохранённых записей</returns>
        public int SaveRealtyPageUrls(IEnumerable<RealtyPageUrl> urls)
        {
            context.RealtyPageUrls.AddRange(urls);
            return context.SaveChanges();
        }
    }
}