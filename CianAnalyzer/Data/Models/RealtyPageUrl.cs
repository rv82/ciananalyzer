﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CianAnalyzer.Data.Models
{
    /// <summary>
    /// Класс для хранения ссылки
    /// </summary>
    public class RealtyPageUrl
    {
        public long Id { get; set; }

        /// <summary>
        /// Идентификатор страницы
        /// </summary>
        [Required]
        public long PageIdentifier { get; set; }

        [Required, StringLength(200)]
        public string PageUrl { get; set; }
    }
}
