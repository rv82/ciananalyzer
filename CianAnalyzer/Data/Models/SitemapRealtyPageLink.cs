﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CianAnalyzer.Data.Models
{
    /// <summary>
    /// Класс для хранения обработанных ссылок
    /// </summary>
    public class SitemapRealtyPageLink
    {
        public long Id { get; set; }

        /// <summary>
        /// Текст ссылки (nullable)
        /// </summary>
        [StringLength(300)]
        public string LinkText { get; set; }

        /// <summary>
        /// Собственно ссылка (URL)
        /// </summary>
        [Required, StringLength(350)]
        public string LinkUrl { get; set; }

        /// <summary>
        /// Находится ли ссылка в обработке
        /// </summary>
        public bool InProcessing { get; set; } = false;

        /// <summary>
        /// Обработана ли ссылка
        /// </summary>
        public bool IsProcessed { get; set; } = false;

        /// <summary>
        /// URL страницы, с которой была взята ссылка
        /// </summary>
        [StringLength(200)]
        public string SitemapUrl { get; set; }
    }
}
