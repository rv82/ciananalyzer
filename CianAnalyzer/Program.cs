﻿using AngleSharp.Dom.Html;
using AngleSharp.Parser.Html;
using CianAnalyzer.Components.Helpers;
using CianAnalyzer.Data;
using CianAnalyzer.Data.Models;
using CianAnalyzer.Data.Services;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using static CianAnalyzer.Components.Constants;

namespace CianAnalyzer
{
    /*
     * Порядок
     *
     * Первая очередь
     * 1. Идём на страницу https://www.cian.ru/sitemap/
     * 2. Разбираем все ссылки из div.pager_pages, получаем список путей, двигаемся по нему (метод UrlHelper.GetLinksFromDocumentBySelector)
     * 3. На каждой странице перебираем ссылки, лежащие в "div.note.objects > div", и кладём их в БД
     *
     * Вторая очередь
     * 4. Переходим на полученный адрес и разбираем ссылки a.cardLink--3KbME (строка уже есть в Constants).
     *    Идентификаторы ссылок складываем в БД.
     *
     * Продумать
     * 1. Как это должно работать в многопоточном режиме
     * 2.
     */

    internal class Program
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        /*
         * 37.48.118.90:13040
         * 83.149.70.159:13040
         */
        private WebProxy webProxy = new WebProxy("83.149.70.159", 13040);

        public bool CheckReCaptcha(IHtmlDocument page)
        {
            // div.rc-anchor.rc-anchor-normal.rc-anchor-light
            var elements = page.QuerySelectorAll("#form_captcha");
            return elements.Length > 0;
        }

        /// <summary>
        /// Получает ссылки со страницы sitemap и записывает их в БД
        /// </summary>
        /// <param name="sitemapPageUrl"></param>
        /// <returns>true, если всё нормально и false, если запись не удалась.</returns>
        public async Task<bool> SaveLinksFromSitemapToDatabase(string sitemapPageUrl)
        {
            bool isSuccess = true;
            using (var dbContext = new AppDbContext())
            using (var transaction = dbContext.Database.BeginTransaction())
            {
                try
                {
                    HtmlParser parser = new HtmlParser();
                    int savedRecordsCount = 0;

                    string sitemapPageText = await PageHelper.GetPageHtmlAsync(sitemapPageUrl, webProxy);
                    IHtmlDocument sitemapPage = await parser.ParseAsync(sitemapPageText);
                    sitemapPage.Location.Href = sitemapPageUrl;
                    if (sitemapPage.StatusCode != HttpStatusCode.OK)
                    {
                        logger.Error(Messages.CannotGetPageHtml);
                        return false;
                    }

                    List<SitemapRealtyPageLink> sitemapLinks = UrlHelper.GetLinkObjectsFromDocumentBySelector(sitemapPage, HtmlElementSelectors.LinkFromSitemapClassName);
                    if (sitemapLinks.Count > 0)
                    {
                        dbContext.SitemapRealtyPageLinks.AddRange(sitemapLinks);
                        savedRecordsCount = dbContext.SaveChanges();
                    }
                    transaction.Commit();
                    logger.Info(string.Format(Messages.LinksFromPageSaved, sitemapPageUrl, savedRecordsCount));
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    logger.Fatal(ex.Message + " Page: " + sitemapPageUrl);
                    logger.Trace(ex.StackTrace);
                    isSuccess = false;
                }
                return isSuccess;
            }
        }

        /// <summary>
        /// Разбирает страницы sitemap, получает с них ссылки и сохраняет в БД.
        /// </summary>
        /// <returns></returns>
        public async Task ParseSitemapAsync()
        {
            HtmlParser parser = new HtmlParser();
            try
            {
                string sitemapPageUrl = string.Join("/", Urls.BaseUrl, Urls.SitemapUrlSegment);
                string sitemapPageText = await PageHelper.GetPageHtmlAsync(sitemapPageUrl, webProxy);
                IHtmlDocument sitemapPage = await parser.ParseAsync(sitemapPageText);
                if (sitemapPage.StatusCode != HttpStatusCode.OK)
                {
                    logger.Error(Messages.CannotGetPageHtml);
                    return;
                }

                List<string> sitemapUrlSegments = UrlHelper.GetLinkSegmentsFromDocumentBySelector(sitemapPage, HtmlElementSelectors.LinkInPagerOnSitemapClassName);
                bool isSuccess;

                // Получаем ссылки с первой страницы
                int attempts = 0;
                do
                {
                    isSuccess = await SaveLinksFromSitemapToDatabase(sitemapPageUrl);
                    if (++attempts > MaxAttemptCount)
                    {
                        break;
                    }
                } while (!isSuccess);

                // Проходим остальные страницы, пропустив первую. Её только что разобрали.
                foreach (string sitemapUrlSegment in sitemapUrlSegments.Skip(1))
                {
                    sitemapPageUrl = string.Join("/", Urls.BaseUrl, sitemapUrlSegment);
                    attempts = 0;
                    do
                    {
                        isSuccess = await SaveLinksFromSitemapToDatabase(sitemapPageUrl);
                        if (++attempts > MaxAttemptCount)
                        {
                            break;
                        }
                    } while (!isSuccess);
                }
            }
            catch (Exception ex)
            {
                logger.Fatal(ex.Message);
                logger.Trace(ex.StackTrace);
            }
        }

        /// <summary>
        /// Разбирает страницы недвижимости, получает с них ссылки и сохраняет в БД.
        /// </summary>
        /// <returns></returns>
        public async Task ParseRealtyPagesAsync()
        {
            /*
             * Правильный алгоритм:
             * 1. Со страницы предложений берём адрес следующей страницы.
             * 2. Со страницы предложений собираем все ссылки "Подробнее".
             * 3. Если ссылка на следующую страницу предложений есть, идём на шаг 1.
             *    Иначе, идём на следующую ссылку из sitemap.
             */
            using (AppDbContext dbContext = new AppDbContext())
            {
                SitemapRealtyPageLink commonRealtyLink = null;
                LinksService linksService = null;
                try
                {
                    HtmlParser parser = new HtmlParser();
                    linksService = new LinksService(dbContext);
                    IHtmlAnchorElement followingPage = null;
                    int savedCount = 0; // Количество сохранённых записей.
                    // Счётчик попыток
                    int attempts = 0;
                    // Берём из БД адрес страницы sitemap
                    commonRealtyLink = linksService.GetOneUnprocessedSitemapRealtyPageLinkAndMarkAsInProcessing();
                    // Если в БД есть ссылки с sitemap, то работаем с ними
                    while (commonRealtyLink != null)
                    {
                        logger.Info("Sitemap: " + commonRealtyLink.SitemapUrl);
                        string nextPageUrl = commonRealtyLink.LinkUrl;
                        // Если случится ошибка, commonRealtyLink != null то, по-хорошему, мы должны
                        // попробовать обработать эту же страницу снова.
                        try
                        {
                            do
                            {
                                // Получаем текст страницы по адресу, извлечённому из БД.
                                string pageText = await PageHelper.GetPageHtmlAsync(nextPageUrl, webProxy);
                                // Объект страницы из текста
                                IHtmlDocument page = await parser.ParseAsync(pageText);
                                // Собираем все ссылки "Подробнее" со страницы.
                                var links = UrlHelper.GetRealtyPageUrlsFromPage(page);
                                if (links.Count == 0 && CheckReCaptcha(page))
                                {
                                    linksService.MarkSitemapRealtyPageLinkAsNotInProcessing(commonRealtyLink.Id);
                                    // For debug
                                    logger.Trace("Sitemap: " + commonRealtyLink.SitemapUrl);
                                    logger.Trace($"Проблема на странице: {commonRealtyLink.LinkUrl}");
                                    logger.Trace(Messages.LinksFromPageSaved, nextPageUrl, savedCount);
                                    Console.ReadKey();
                                    Environment.Exit(0);
                                }
                                // Сохраняем ссылки в БД.
                                savedCount = linksService.SaveRealtyPageUrls(links);
                                if (savedCount == links.Count)
                                {
                                    logger.Info(Messages.LinksFromPageSaved, nextPageUrl, savedCount);
                                }
                                else
                                {
                                    logger.Info(Messages.LinksFromPageSavedOf, nextPageUrl, savedCount, links.Count);
                                }

                                // Получаем ссылку на следующую страницу от текущей
                                followingPage = page.QuerySelector(HtmlElementSelectors.LinkFollowingToCurrent) as IHtmlAnchorElement;
                                // если null, то следующих страниц нет. Можно переходить на следующую страницу sitemap
                                if (followingPage != null)
                                {
                                    nextPageUrl = followingPage.Href;
                                }
                            } while (followingPage != null);

                            linksService.MarkSitemapRealtyPageLinkAsProcessed(commonRealtyLink.Id);
                            commonRealtyLink = linksService.GetOneUnprocessedSitemapRealtyPageLinkAndMarkAsInProcessing();
                            attempts = 0;
                        }
                        catch (Exception ex)
                        {
                            logger.Fatal(ex.Message + " Page: " + commonRealtyLink?.LinkUrl);
                            logger.Trace(ex.StackTrace + " Page: " + commonRealtyLink?.LinkUrl);
                            // Если количество попыток больше заданного, переходим к следующей странице
                            if (++attempts > MaxAttemptCount)
                            {
                                commonRealtyLink = linksService.GetOneUnprocessedSitemapRealtyPageLinkAndMarkAsInProcessing();
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    logger.Fatal(ex.Message);
                    logger.Trace(ex.StackTrace);
                    if (linksService != null && commonRealtyLink != null)
                    {
                        linksService.MarkSitemapRealtyPageLinkAsNotInProcessing(commonRealtyLink.Id);
                    }
                }
            }
        }

        public static void Main(string[] args)
        {
            var program = new Program();
            Console.WriteLine(InfoMessages.FetchSitemapLinks);
            var key = Console.ReadKey();
            if (key.KeyChar == 'y' || key.KeyChar == 'Y')
            {
                Console.WriteLine(InfoMessages.FetchingSitemapLinks);
                program.ParseSitemapAsync().Wait();
            }

            Console.WriteLine(InfoMessages.FetchRealtyLinks);
            key = Console.ReadKey();
            if (key.KeyChar == 'y' || key.KeyChar == 'Y')
            {
                Console.WriteLine(InfoMessages.FetchingRealtyLinks);
                Task[] tasks = new Task[TasksCount];
                for (int i = 0; i < TasksCount; i++)
                {
                    tasks[i] = program.ParseRealtyPagesAsync();
                }
                Task.WaitAll(tasks);
            }

            Console.WriteLine(InfoMessages.PressAnyKey);
            Console.ReadKey();
        }
    }
}