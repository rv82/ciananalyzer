﻿using AngleSharp.Dom;
using AngleSharp.Dom.Html;
using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using static CianAnalyzer.Components.Constants;

namespace CianAnalyzer.Components.Helpers
{
    public static class PageHelper
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Получает текст страницы с заданным URL
        /// </summary>
        /// <param name="url"></param>
        /// <param name="webProxy"></param>
        /// <returns></returns>
        public static string GetPageHtml(string url, WebProxy webProxy = null)
        {
            string page = null;

            HttpWebRequest request;
            HttpWebResponse response = null;


            int attempts = 0;
            bool isSuccess = false;

            while (!isSuccess)
            {
                try
                {
                    request = (HttpWebRequest)WebRequest.Create(url);
                    request.Method = RequestSettings.Method;
                    request.Timeout = RequestSettings.Timeout;
                    request.Accept = RequestSettings.AllowedHeaders;
                    request.UserAgent = RequestSettings.UserAgent;
                    request.KeepAlive = true;
                    request.AllowAutoRedirect = true;
                    if (webProxy != null)
                    {
                        request.Proxy = webProxy;
                    }

                    response = (HttpWebResponse)request.GetResponse();
                    isSuccess = true;
                    //catch (Exception ex)
                    //{
                    //    logger.Info("Попытка " + attempts);
                    //    if (++attempts > MaxAttemptCount)
                    //    {
                    //        throw;
                    //    }
                    //}

                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        Stream stream = response.GetResponseStream();

                        using (response)
                        using (stream)
                        {
                            StreamReader reader;
                            if (response.CharacterSet == null)
                            {
                                reader = new StreamReader(stream);
                            }
                            else
                            {
                                reader = new StreamReader(stream, Encoding.GetEncoding(response.CharacterSet));
                            }
                            using (reader)
                            {
                                page = reader.ReadToEnd();
                            }
                        }
                    }
                    isSuccess = true;
                }
                catch(Exception ex)
                {
                    request = null;
                    response = null;
                    logger.Info("Попытка " + attempts);
                    if (++attempts > MaxAttemptCount)
                    {
                        throw;
                    }
                }
            }
            return page;
        }

        #region Асинхронные методы
        /// <summary>
        /// Асинхронно получает текст страницы с заданным URL
        /// </summary>
        /// <param name="url"></param>
        /// <param name="webProxy"></param>
        /// <returns></returns>
        public static Task<string> GetPageHtmlAsync(string url, WebProxy webProxy = null)
        {
            return Task.Run(() => GetPageHtml(url, webProxy));
        }
        #endregion Асинхронные методы
    }
}
