﻿using AngleSharp.Dom;
using AngleSharp.Dom.Html;
using CianAnalyzer.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using static CianAnalyzer.Components.Constants;

namespace CianAnalyzer.Components.Helpers
{
    public static class UrlHelper
    {
        #region Работа со ссылками

        /// <summary>
        /// Возвращает список ссылок из документа по селектору
        /// </summary>
        /// <param name="document">HTML-документ</param>
        /// <param name="selector">селектор</param>
        /// <returns></returns>
        public static List<string> GetLinkSegmentsFromDocumentBySelector(IHtmlDocument document, string selector)
        {
            List<string> sitemapUrlSegments = new List<string>();
            IHtmlCollection<IElement> pageLinks = document.QuerySelectorAll(selector);
            string url;
            foreach (IHtmlAnchorElement link in pageLinks)
            {
                url = link.PathName + link.Search;
                sitemapUrlSegments.Add(url.Trim('/'));
            }
            return sitemapUrlSegments;
        }

        public static List<SitemapRealtyPageLink> GetLinkObjectsFromDocumentBySelector(IHtmlDocument document, string selector)
        {
            List<SitemapRealtyPageLink> sitemapLinks = new List<SitemapRealtyPageLink>();
            IHtmlCollection<IElement> pageLinks = document.QuerySelectorAll(selector);
            foreach (IHtmlAnchorElement link in pageLinks)
            {
                sitemapLinks.Add(new SitemapRealtyPageLink
                {
                    LinkText = link.InnerHtml,
                    LinkUrl = string.Join("/", Urls.BaseUrl, link.PathName.Trim('/')),
                    SitemapUrl=document.Location.Href
                });
            }
            return sitemapLinks;
        }

        /// <summary>
        /// Возвращает идентификатор страницы для из переданного URL
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static long GetPageIdentifierFromUrl(string url)
        {
            Uri uri = new Uri(url);
            return Convert.ToInt64(uri.Segments.Last().Trim('/'));
        }

        /// <summary>
        /// Получает список идентификаторов ссылок со страницы
        /// </summary>
        /// <param name="document"></param>
        /// <returns></returns>
        public static List<long> GetRealtyPageUrlIdentifiersFromPage(IHtmlDocument document)
        {
            List<long> list = new List<long>();
            IHtmlCollection<IElement> links = document.QuerySelectorAll(HtmlElementSelectors.TargetLinkClassName);
            foreach (IHtmlAnchorElement link in links)
            {
                list.Add(GetPageIdentifierFromUrl(link.Href));
            }
            return list;
        }

        /// <summary>
        /// Возвращает список ссылок со страницы.
        /// </summary>
        /// <param name="document"></param>
        /// <returns>Список ссылок со страницы.</returns>
        public static List<RealtyPageUrl> GetRealtyPageUrlsFromPage(IHtmlDocument document)
        {
            List<RealtyPageUrl> list = new List<RealtyPageUrl>();
            IHtmlCollection<IElement> links = document.QuerySelectorAll(HtmlElementSelectors.TargetLinkClassName);
            foreach (IHtmlAnchorElement link in links)
            {
                list.Add(new RealtyPageUrl
                {
                    PageUrl = link.Href,
                    PageIdentifier = GetPageIdentifierFromUrl(link.Href)
                });
            }
            return list;
        }



        #endregion Работа со ссылками
    }
}