﻿namespace CianAnalyzer.Components
{
    /// <summary>
    /// Constants
    /// </summary>
    public static class Constants
    {
        /// <summary>
        /// Наименование соединения
        /// </summary>
        public const string ConnectionName = "DbConnection";

        /// <summary>
        /// Количество попыток разбора страниц
        /// </summary>
        public const int MaxAttemptCount = 10;

        public const int TasksCount = 4;

        public static class Urls
        {
            public const string BaseUrl = "https://www.cian.ru";
            public const string SitemapUrlSegment = "sitemap";
        }

        /// <summary>
        /// Настройки HTTP-запроса
        /// </summary>
        public static class RequestSettings
        {
            public const string Method = "GET";
            public const string AllowedHeaders = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8";
            public const string UserAgent = "Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36";
            public const int Timeout = 10000;
        }

        /// <summary>
        /// Действия: купить или снять
        /// </summary>
        public static class Actions
        {
            public const string Buy = "kupit";
            public const string Rent = "snyat";
        }

        /// <summary>
        /// Сообщения об ошибках
        /// </summary>
        public static class Messages
        {
            // Сообщения об ошибках
            public const string CannotGetPageHtml = "Не могу получить HTML-страницу.";

            // Другие сообщения
            public const string LinksFromPageSaved = "Страница: {0}. Сохранено ссылок: {1}.";
            public const string LinksFromPageSavedOf = "Страница: {0}. Сохранено ссылок: {1} из {2}.";
        }

        /// <summary>
        /// Информационные сообщения
        /// </summary>
        public static class InfoMessages
        {
            public const string PressAnyKey = "Нажмите любую клавишу...";
            public const string FetchSitemapLinks = "\nПолучить ссылки с sitemap?\nY - да, другая клавиша - нет";
            public const string FetchRealtyLinks = "\nПолучить ссылки со страниц предложений?\nY - да, другая клавиша - нет";

            public const string FetchingSitemapLinks = "\nПолучаем ссылки с sitemap.\n";
            public const string FetchingRealtyLinks = "\nПолучаем ссылки со страниц предложений.\n";
        }

        /// <summary>
        /// Атрибуты html-эелементов
        /// </summary>
        public static class AttributeNames
        {
            public const string Href = "href";
        }

        /// <summary>
        /// Классы html-эелементов
        /// </summary>
        public static class HtmlElementSelectors
        {
            public const string TargetLinkClassName = "a.cardLink--3KbME";
            public const string LinkInPagerOnSitemapClassName = "div.pager_pages a";
            public const string LinkFromSitemapClassName = "div.note.objects > div a";
            public const string RealtyPageLinkClassName = "ul[class^='list--'] a";
            public const string LinkFollowingToCurrent = "ul[class^='list--'] li[class*='list-item--active--'] + li > a";
        }

        /// <summary>
        /// SQL-запросы
        /// </summary>
        public static class SqlQueries
        {
            public const string SaveProcessedLinkQuery = "insert into dbo.ProcessedLinks(LinkText,LinkUrl,InProcessing,IsProcessed) values (@LinkText,@LinkUrl,@InProcessing,@IsProcessed)";
        }

        /// <summary>
        /// Имена параметров для SQL-запросов
        /// </summary>
        public static class SqlParameterNames
        {
            public const string LinkText = "@LinkText";
            public const string LinkUrl = "@LinkUrl";
            public const string InProcessing = "@InProcessing";
            public const string IsProcessed = "@IsProcessed";
        }
    }
}