﻿CianAnalyzer
===

Подготовка
---

0. Если в проекте отсутствует namespace Migrations (папка Migrations в дереве файлов), то
нужно открыть package manager console в Visual Studio
и выполнить команды (в противном случае пропустить этот шаг):

 - Enable-Migrations

 - Add-Migration Database

1. Чтобы создать БД, выполнить команду:

 - Update-Database

2. Запустить скрипты из каталога sql
