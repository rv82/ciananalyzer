﻿Купить
===

|  region   |  domain  | action |        subject        |  regionName   | regionSubject |
| --------- | -------- | ------ | --------------------- | ------------- | ------------- |
| *Область* |          |        |                       |               |               |
| kemerovo. | cian.ru/ | kupit- | kvartiru-             | kemerovskaya- | oblast        |
| kemerovo. | cian.ru/ | kupit- | kvartiru-vtorichka-   | kemerovskaya- | oblast        |
| kemerovo. | cian.ru/ | kupit- | kvartiru-novostroyki- | kemerovskaya- | oblast        |
| kemerovo. | cian.ru/ | kupit- | komnatu-              | kemerovskaya- | oblast        |
| kemerovo. | cian.ru/ | kupit- | dom-                  | kemerovskaya- | oblast        |
| kemerovo. | cian.ru/ | kupit- | chast-doma-           | kemerovskaya- | oblast        |
| kemerovo. | cian.ru/ | kupit- | taunhaus-             | kemerovskaya- | oblast        |
| kemerovo. | cian.ru/ | kupit- | zemelniy-uchastok-    | kemerovskaya- | oblast        |

Снять
===

|    region    |  domain  | action |  subject  |   regionName   | regionSubject |
| ------------ | -------- | ------ | --------- | -------------- | ------------- |
| *Область*    |          |        |           |                |               |
| kemerovo.    | cian.ru/ | snyat- | kvartiru- | kemerovskaya-  | oblast        |
| kemerovo.    | cian.ru/ | snyat- | komnatu-  | kemerovskaya-  | oblast        |
| kemerovo.    | cian.ru/ | snyat- | dom-      | kemerovskaya-  | oblast        |
| *Край*       |          |        |           |                |               |
| krasnoyarsk. | cian.ru/ | snyat- | kvartiru- | krasnoyarskiy- | kray          |
| *Республика* |          |        |           |                |               |
| www.         | cian.ru/ | snyat- | kvartiru- | adygeya        |               |

kemerovo.cian.ru/cat.php?deal_type=rent&engine_version=2&object_type%5B0%5D=4&offer_type=suburban&region=4580&type=4 - часть дома (object_type%5B0%5D=4)

kemerovo.cian.ru/cat.php?deal_type=rent&engine_version=2&object_type%5B0%5D=2&offer_type=suburban&region=4580&type=4 - таунхаус (object_type%5B0%5D=2)

Саха (Якутия)
www.cian.ru/cat.php?deal_type=rent&engine_version=2&offer_type=flat&region=4610&type=4

Снять коммерческую недвижимость
===

kemerovo.cian.ru/cat.php?deal_type=rent&engine_version=2&offer_type=offices&office_type%5B0%5D=1&office_type%5B10%5D=12&office_type%5B1%5D=2&office_type%5B2%5D=3&office_type%5B3%5D=4&office_type%5B4%5D=5&office_type%5B5%5D=6&office_type%5B6%5D=7&office_type%5B7%5D=9&office_type%5B8%5D=10&office_type%5B9%5D=11&region=4580

Нестандартные адреса для жилья
===

Еврейская АО
www.cian.ru/cat.php?deal_type=sale&engine_version=2&offer_type=flat&region=4569

Ингушетия
www.cian.ru/cat.php?deal_type=sale&engine_version=2&offer_type=flat&region=4571

Крым (добавлен)
krym.cian.ru/kupit-kvartiru/

Ненецкий АО
www.cian.ru/cat.php?deal_type=sale&engine_version=2&offer_type=flat&region=4595

Тыва
www.cian.ru/cat.php?deal_type=sale&engine_version=2&offer_type=flat&region=4622

Чукотский АО
www.cian.ru/cat.php?deal_type=sale&engine_version=2&offer_type=flat&region=4634

Субъекты
===

Доля
kostroma.cian.ru/cat.php?deal_type=sale&engine_version=2&offer_type=flat&region=4583&room8=1

Коммерческая недвижимость
===

kostroma.cian.ru/cat.php?deal_type=sale&engine_version=2&offer_type=offices&office_type[0]=1&office_type[10]=12&office_type[1]=2&office_type[2]=3&office_type[3]=4&office_type[4]=5&office_type[5]=6&office_type[6]=7&office_type[7]=9&office_type[8]=10&office_type[9]=11&region=4583